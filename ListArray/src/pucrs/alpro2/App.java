package pucrs.alpro2;

import java.util.Iterator;

public class App {

	public static void main(String[] args) {
		ListArray<Integer> lista = new ListArray<>();
		System.out.println("Teste de ListArray");
		System.out.println("Tam: "+lista.size());
		lista.add(1);
		lista.add(2);
		lista.add(4);
		lista.add(5);
		lista.add(6);
		System.out.println("Tam: "+lista.size());
		lista.add(2, 3); // insere o valor 3 na pos. 2
		lista.add(0, 0); // insere o valor 0 no início
		lista.add(7);
		lista.add(8);
		lista.add(90); // teste de conflito
		
		lista.remove(8);  // apaga o 8
		lista.remove(8);  // apaga o 90
		
		for(int i=0; i<lista.size(); i++)
			System.out.println(lista.get(i));
		
		System.out.println("Com iterador:");
		Iterator<Integer> it = lista.iterator();
		while(it.hasNext()) {
			System.out.println(it.next());
		}
		
		System.out.println("Mesma coisa, mas com for...each:");
		for(int valor: lista)
			System.out.println(valor);
		
		System.out.println("Com iterador reverso:");
		Iterator<Integer> it2 = lista.reverseIterator();
		while(it2.hasNext()) {
			System.out.println(it2.next());
		}
		
		System.out.println("Tam: "+lista.size());
		System.out.println();
		System.out.println(lista);
	}
}
