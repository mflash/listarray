package pucrs.alpro2;

import java.util.Iterator;
import java.util.NoSuchElementException;

public class ListArray<E> implements ListTAD<E>, Iterable<E> {

	private Object[] data;
	private int count;

	public ListArray() {
		this(10); // chama o construtor com tamanho 10 (default)
	}

	public ListArray(int c) {
		data = new Object[c];
		count = 0;
	}

	@Override
	public void add(E e) {
		// TODO Auto-generated method stub
		if (count == data.length)
			setCapacity(data.length * 2);
		data[count] = e;
		count++;
	}

	private void setCapacity(int newCapacity) {
		if (newCapacity != data.length) {
			int min = 0;
			Object[] newData = new Object[newCapacity]; // B

			// min armazena a menor capacidade: atual ou nova
			min = data.length < newCapacity ? data.length : newCapacity;

			// Copia os elementos de data para newData
			for (int i = 0; i < min; i++)
				newData[i] = data[i];

			// Finalmente, newData passa a ser o array em uso
			data = newData; // A = B
		}
	}

	@Override
	public void add(int index, E element) {
		if(index < 0 || index >= count)
			throw new IndexOutOfBoundsException("Posição inválida!");
		if (count == data.length)
			setCapacity(data.length * 2);
		for(int pos=count; pos > index; pos--)
			data[pos] = data[pos-1];
		data[index] = element;
		count++;
	}

	@Override
	public E get(int index) {
		if(index < 0 || index >= count)
			throw new IndexOutOfBoundsException("Posição inválida!");
		return (E) data[index];
	}

	@Override
	public int indexOf(E e) {
		// Procura elemento no array, se achar retorna
	    // Cuidado: uma exceção ocorrerá se houver um null no array!
	    for(int p=0; p<count; p++)
	      if(data[p].equals(e))
	        return p;
	    // Neste ponto, não achou: retorna -1
	    return -1;
	}

	@Override
	public void set(int index, E element) {
		if(index < 0 || index >= count)
			throw new IndexOutOfBoundsException("Posição inválida!");
		data[index] = element;
	}

	@Override
	public boolean remove(E e) {
		int pos = indexOf(e);
		if(pos != -1) {
			remove(pos);
			return true;
		}
		return false;
	}

	@Override
	public E remove(int index) {
		if(index < 0 || index >= count)
			throw new IndexOutOfBoundsException("Posição inválida!");
		E aux = (E) data[index];
		
		for(int pos=index; pos < count-1; pos++)
			data[pos] = data[pos+1];
		data[count-1] = null; // apaga ultimo
		count--;
		
		return aux;
	}

	@Override
	public boolean isEmpty() {
		// TODO Auto-generated method stub
		return false;
	}

	@Override
	public int size() {
		// TODO Auto-generated method stub
		return count;
	}

	@Override
	public boolean contains(E e) {
		// Procura elemento no array, se achar retorna
	    // Cuidado: uma exceção ocorrerá se houver um null no array!
		return indexOf(e) != -1;
	}

	@Override
	public void clear() {
		data = new Object[10];
		count = 0;
	}
	
	@Override
	public String toString() {
		StringBuffer aux = new StringBuffer();
		aux.append("[ ");
		for(int pos=0; pos<count; pos++)
		{
			aux.append(data[pos]+" ");
		}
		aux.append(" ]");
		return aux.toString();
	}

	@Override
	public Iterator iterator() {
		return new Iterator<E>() {

			private int position = 0;
			@Override
			public boolean hasNext() {
				return position != count;
			}

			@Override
			public E next() {
				if(position == count)
					throw new NoSuchElementException();
				E item = (E) data[position];
				position++;
				return item;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();				
			}
		};
	}
	
	public Iterator reverseIterator() {
		return new Iterator<E>() {

			private int position = count-1;
			@Override
			public boolean hasNext() {
				return position != -1;
			}

			@Override
			public E next() {
				if(position == -1)
					throw new NoSuchElementException();
				E item = (E) data[position];
				position--;
				return item;
			}

			@Override
			public void remove() {
				throw new UnsupportedOperationException();				
			}
		};
	}
	
	
	
	
	

}
